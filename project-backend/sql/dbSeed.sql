
CREATE DATABASE news;
USE news;

insert into category (id, name) values (1,"Zivot");
insert into category (id, name) values (2,"Kultura");
insert into category (id, name) values (3,"Sport");

insert into news(title,category_id,description, content) values ("Opasnost iz svemira",1,"Astedroid u obliku lobanje trebalo bi da po drugi put prodje pored Zemlje do Novembra 2018.godine", "Sadrzaj vesti.....");
insert into news(title,category_id,description, content) values ("Neobljavljeni album grupe EKV: zatvaranje kruga",2,"Album 'Krug' poslednje zvucno svedocanstvo grupe 'EKV' sa akusticnog koncerta odrzanog u maju 1994.godine u Pristini, svega pola godine pre smrti Milana Mladenovica, dugo se smatrao izgubljen.", "Sadrzaj vesti.....");
insert into news(title,category_id,description, content) values ("Zemlje je suplja u njoj zive Vikinzi",1,"Nova teorija zavere mozda jedna od najbizarnijih. Prema njoj, Zemlja je suplja, a u njoj zive Vikinzi i superiorna rasa 'vanzemaljaca-ljudi', pise Dejli mejl.", "Sadrzaj vesti.....");
insert into news(title,category_id,description, content) values ("Sve sto ste culi o Mesecu je laz!",1,"Bivsi pilot CIA tvrdi da na Mesecu zivi 250 miliona stanovnika, ima i dokaze za to.", "Sadrzaj vesti.....");
insert into news(title,category_id,description, content) values ("Zvanicno je: Ovo su najdosadniji filmovi ikada snimljeni",2,"Britanci su zamolili vodece filmske kriticare da naprave sboje liste najdosadnijih filmova koje su pregledali (ili nusu!), a onda su tome dodali glasove jos 2000 gradjana. Rezultat je prilozena lista. Da li se slazete sa njom?", "Sadrzaj vesti.....");

insert into comment(id, text, news_id, security_user_id) values (1, 'super je projekat', 1, 1);
insert into comment(id, text, news_id, security_user_id) values (2, 'super je stvarno', 2, 1);
insert into comment(id, text, news_id, security_user_id) values (3, 'extra', 3, 1);



-- insert users
-- password is 12345 (bcrypt encoded) 
insert into security_user (username, password ,role) values 
	('admin', '$2a$04$4pqDFh9SxLAg/uSH59JCB.LwIS6QoAjM9qcE7H9e2drFuWhvTnDFi', 'Admin');
-- password is abcdef (bcrypt encoded)
insert into security_user (username, password,  role) values 
	('Pera', '$2a$04$Yr3QD6lbcemnrRNLbUMLBez2oEK15pdacIgfkvymQ9oMhqsEE56EK', 'User');
