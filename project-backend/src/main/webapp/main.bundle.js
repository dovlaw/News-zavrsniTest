webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<div>\n  <nav class=\"navbar navbar-inverse\">\n    <div class=\"container\">\n      <div id=\"navbar\" class=\"navbar-collapse collapse\">\n        <ul class=\"nav navbar-nav\">\n          <li>\n            <a href=\"menu\">VX</a> \n          </li>\n          <li class=\"active\">\n            <a href=\"menu\">Home</a>\n          </li>\n          <li>\n            <a href=\"about\">About</a>\n          </li>\n          <li>\n            <a href=\"contact\">Contact</a>\n          </li>\n        </ul>\n        <ul class=\"nav navbar-nav pull-right\">\n          <li *ngIf=\"isLoggedIn()\">\n            <a (click)=\"logout()\">Logout, {{user.username}}</a>\n          </li>\n          <li *ngIf=\"!isLoggedIn()\">\n            <a [routerLink]=\"['/login']\">Login</a>\n          </li>\n          <li *ngIf=\"!isLoggedIn()\">\n            <a [routerLink]=\"['/register']\">Register</a>\n          </li>\n        </ul>\n      </div>\n    </div>\n  </nav>\n\n  <div class=\"container theme-showcase\" role=\"main\">\n    <div class=\"jumbotron\">\n      <h1>VX Group</h1>\n      <p>Portal aktuelnih vesti</p>\n    </div>\n    <router-outlet></router-outlet>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var AppComponent = (function () {
    function AppComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.title = "";
        this.user = "";
    }
    AppComponent.prototype.logout = function () {
        this.authenticationService.logout();
        this.router.navigate(["/login"]);
    };
    AppComponent.prototype.isLoggedIn = function () {
        var res = this.authenticationService.isLoggedIn();
        if (res) {
            this.getCurrentUsername();
        }
        return res;
    };
    AppComponent.prototype.getCurrentUsername = function () {
        var res = this.authenticationService.getCurrentUser();
        if (res) {
            this.user = res;
        }
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: "app-root",
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_service_1.AuthenticationService, router_1.Router])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;


/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var platform_browser_1 = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
var core_1 = __webpack_require__("../../../core/esm5/core.js");
// import { HttpModule } from "@angular/http";
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var forms_1 = __webpack_require__("../../../forms/esm5/forms.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var app_component_1 = __webpack_require__("../../../../../src/app/app.component.ts");
var login_component_1 = __webpack_require__("../../../../../src/app/components/login/login.component.ts");
var register_component_1 = __webpack_require__("../../../../../src/app/components/register/register.component.ts");
var add_component_1 = __webpack_require__("../../../../../src/app/components/add/add.component.ts");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var jwt_utils_service_1 = __webpack_require__("../../../../../src/app/services/jwt-utils.service.ts");
var token_interceptor_service_1 = __webpack_require__("../../../../../src/app/services/token-interceptor.service.ts");
var can_activate_auth_guard_1 = __webpack_require__("../../../../../src/app/services/can-activate-auth.guard.ts");
var news_service_service_1 = __webpack_require__("../../../../../src/app/services/news-service.service.ts");
var category_service_service_1 = __webpack_require__("../../../../../src/app/services/category-service.service.ts");
var news_list_component_1 = __webpack_require__("../../../../../src/app/components/news-list/news-list.component.ts");
var edit_component_1 = __webpack_require__("../../../../../src/app/components/edit/edit.component.ts");
var filter_component_1 = __webpack_require__("../../../../../src/app/components/filter/filter.component.ts");
var news_list_item_component_1 = __webpack_require__("../../../../../src/app/components/news-list-item/news-list-item.component.ts");
var appRoutes = [
    { path: "login", component: login_component_1.LoginComponent },
    { path: "register", component: register_component_1.RegisterComponent },
    { path: "menu", component: news_list_component_1.NewsListComponent },
    { path: "add", component: add_component_1.Add, canActivate: [can_activate_auth_guard_1.CanActivateAuthGuard] },
    { path: "edit/:id", component: edit_component_1.Edit, canActivate: [can_activate_auth_guard_1.CanActivateAuthGuard] },
    { path: "", redirectTo: "menu", pathMatch: "full" },
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                app_component_1.AppComponent,
                login_component_1.LoginComponent,
                register_component_1.RegisterComponent,
                news_list_component_1.NewsListComponent,
                add_component_1.Add,
                edit_component_1.Edit,
                filter_component_1.FilterComponent,
                news_list_item_component_1.NewsListItemComponent
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                forms_1.FormsModule,
                router_1.RouterModule.forRoot(appRoutes, {
                    enableTracing: false
                })
            ],
            providers: [
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: token_interceptor_service_1.TokenInterceptorService,
                    multi: true
                },
                authentication_service_service_1.AuthenticationService,
                can_activate_auth_guard_1.CanActivateAuthGuard,
                jwt_utils_service_1.JwtUtilsService,
                category_service_service_1.CategoryService,
                news_service_service_1.NewsService,
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;


/***/ }),

/***/ "../../../../../src/app/components/add/add.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/add/add.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"form\" (ngSubmit)=\"add()\" *ngIf=\"dataLoaded\" ngNativeValidate>\n    <div class=\"form-group\">\n      <label>Category</label>\n      <select required name=\"category\" class=\"form-control\"  [compareWith]=\"byId\" [(ngModel)]=\"news.category\">\n        <option *ngFor=\"let category of categories\" [ngValue]=\"category\">{{category.name}}</option>  \n      </select> \n    </div>\n   \n    <div class=\"form-group\">\n      <label>Naslov</label>\n      <input required type=\"text\" name=\"name\" class=\"form-control\" [(ngModel)]=\"news.title\"/>\n    </div>\n    <div class=\"form-group\">\n      <label>Opis</label>\n      <input required type=\"text\" name=\"price\" class=\"form-control\" [(ngModel)]=\"news.description\"/>\n    </div>\n    <div class=\"form-group\">\n      <label>Sadrzaj</label>\n      <input required type=\"text\" name=\"price\" class=\"form-control\" [(ngModel)]=\"news.content\" />\n    </div>\n    <div class=\"pull-right\">\n      <input type=\"submit\" class=\"btn btn-primary\" value=\"save\"/>\n      <button class=\"btn btn-warning\" [routerLink]=\"['/menu']\">Back to the list</button>      \n    </div>\n  </form>\n  "

/***/ }),

/***/ "../../../../../src/app/components/add/add.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var category_service_service_1 = __webpack_require__("../../../../../src/app/services/category-service.service.ts");
var news_service_service_1 = __webpack_require__("../../../../../src/app/services/news-service.service.ts");
var Add = (function () {
    function Add(categoryService, newsService, router) {
        this.categoryService = categoryService;
        this.newsService = newsService;
        this.router = router;
        this.dataLoaded = false;
    }
    Add.prototype.ngOnInit = function () {
        this.loadData();
    };
    Add.prototype.loadData = function () {
        var _this = this;
        this.categoryService.getAll().subscribe(function (data) {
            _this.categories = data;
            _this.dataLoaded = true;
        }, function (error) {
            console.log(error);
        });
        this.news = {
            category: null,
            title: "",
            description: "",
            content: ""
        };
    };
    Add.prototype.add = function () {
        this.newsService.save(this.news).subscribe(function (yes) {
        }, function (no) {
            alert("Error");
        });
        this.news = {
            category: null,
            title: "",
            description: "",
            content: ""
        };
        var res = confirm("Press ok to add more or cancel to go back to the list?");
        if (!res) {
            this.router.navigate(["/menu"]);
        }
    };
    Add.prototype.byId = function (cat1, cat2) {
        if (cat1 && cat2) {
            return cat1.id === cat2.id;
        }
    };
    Add = __decorate([
        core_1.Component({
            selector: "app-add",
            template: __webpack_require__("../../../../../src/app/components/add/add.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/add/add.component.css")]
        }),
        __metadata("design:paramtypes", [category_service_service_1.CategoryService,
            news_service_service_1.NewsService,
            router_1.Router])
    ], Add);
    return Add;
}());
exports.Add = Add;


/***/ }),

/***/ "../../../../../src/app/components/edit/edit.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/edit/edit.component.html":
/***/ (function(module, exports) {

module.exports = "<form class=\"form\" (ngSubmit)=\"save()\" *ngIf=\"dataLoaded\" ngNativeValidate>\n  <div class=\"form-group\">\n    <label>Kategorije</label>\n    <select required name=\"category\" class=\"form-control\"  [compareWith]=\"byId\" [(ngModel)]=\"news.category\">\n      <option *ngFor=\"let category of categories\" [ngValue]=\"category\">{{category.name}}</option>  \n    </select> \n  </div>\n \n  <div class=\"form-group\">\n    <label>Naslov</label>\n    <input required type=\"text\" name=\"name\" class=\"form-control\" [(ngModel)]=\"news.title\"/>\n  </div>\n  <div class=\"form-group\">\n    <label>Opis</label>\n    <input required type=\"text\" name=\"price\" class=\"form-control\" [(ngModel)]=\"news.description\"/>\n  </div>\n  <div class=\"pull-right\">\n    <input type=\"submit\" class=\"btn btn-primary\" value=\"save\"/>\n    <button class=\"btn btn-warning\" [routerLink]=\"['/menu']\">Povratak na glavni meni</button>      \n  </div>\n  </form>\n  "

/***/ }),

/***/ "../../../../../src/app/components/edit/edit.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var category_service_service_1 = __webpack_require__("../../../../../src/app/services/category-service.service.ts");
var news_service_service_1 = __webpack_require__("../../../../../src/app/services/news-service.service.ts");
var Edit = (function () {
    function Edit(categoryService, newsService, router, route) {
        this.categoryService = categoryService;
        this.newsService = newsService;
        this.router = router;
        this.route = route;
        this.dataLoaded = false;
    }
    Edit.prototype.ngOnInit = function () {
        this.loadData();
    };
    Edit.prototype.loadData = function () {
        var _this = this;
        this.categoryService.getAll().subscribe(function (data) { return _this.categories = data; });
        this.route.params.subscribe(function (data) {
            _this.newsService.get(data["id"]).subscribe(function (data) {
                _this.news = data;
                _this.dataLoaded = true;
            });
        });
    };
    Edit.prototype.save = function () {
        var _this = this;
        this.newsService.save(this.news).subscribe(function (yes) {
            _this.router.navigate(["/menu"]);
        }, function (no) {
            alert("Error");
        });
    };
    Edit.prototype.byId = function (new1, new2) {
        if (new1 && new2) {
            return new1.id === new2.id;
        }
    };
    Edit = __decorate([
        core_1.Component({
            selector: "app-edit",
            template: __webpack_require__("../../../../../src/app/components/edit/edit.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/edit/edit.component.css")]
        }),
        __metadata("design:paramtypes", [category_service_service_1.CategoryService,
            news_service_service_1.NewsService,
            router_1.Router,
            router_1.ActivatedRoute])
    ], Edit);
    return Edit;
}());
exports.Edit = Edit;


/***/ }),

/***/ "../../../../../src/app/components/filter/filter.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/filter/filter.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\" *ngIf=\"dataLoaded\">\n  <div class=\"row\">\n    <label>Kategorija</label>\n    <select required name=\"categoryId\" class=\"inline\" [(ngModel)]=\"filter.categoryId\" (change)=\"selectionChanged()\">\n      <option [ngValue]=\"0\">All</option>\n      <option *ngFor=\"let category of categories\" [ngValue]=\"category.id\">{{category.name}}</option>\n    </select>\n    <label>Naslov</label>\n    <input name=\"name\" class=\"input inline\" (input)=\"selectionChanged()\" [(ngModel)]=\"filter.name\" />\n    \n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/filter/filter.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var news_service_service_1 = __webpack_require__("../../../../../src/app/services/news-service.service.ts");
var core_2 = __webpack_require__("../../../core/esm5/core.js");
var core_3 = __webpack_require__("../../../core/esm5/core.js");
var category_service_service_1 = __webpack_require__("../../../../../src/app/services/category-service.service.ts");
var FilterComponent = (function () {
    function FilterComponent(newsService, categoryService) {
        this.newsService = newsService;
        this.categoryService = categoryService;
        this.filterNews = new core_3.EventEmitter();
        this.filter = {
            title: "",
            categoryId: 0
        };
        this.dataLoaded = false;
    }
    FilterComponent.prototype.ngOnInit = function () {
        this.loadData();
    };
    FilterComponent.prototype.loadData = function () {
        var _this = this;
        this.categoryService.getAll().subscribe(function (data) {
            _this.categories = data;
            _this.dataLoaded = true;
        });
    };
    FilterComponent.prototype.selectionChanged = function () {
        this.filterNews.next(this.filter);
    };
    __decorate([
        core_2.Output(),
        __metadata("design:type", core_3.EventEmitter)
    ], FilterComponent.prototype, "filterNews", void 0);
    FilterComponent = __decorate([
        core_1.Component({
            selector: "app-filter",
            template: __webpack_require__("../../../../../src/app/components/filter/filter.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/filter/filter.component.css")],
        }),
        __metadata("design:paramtypes", [news_service_service_1.NewsService,
            category_service_service_1.CategoryService])
    ], FilterComponent);
    return FilterComponent;
}());
exports.FilterComponent = FilterComponent;


/***/ }),

/***/ "../../../../../src/app/components/login/login.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <form class=\"form-signin\" (ngSubmit)=\"login()\">\n      <label for=\"username\" class=\"sr-only\">Username: </label>\n      <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.username\" placeholder=\"Username\"\n        required autofocus/>\n      <label for=\"pass\" class=\"sr-only\">Password</label>\n      <br/>\n      <input type=\"password\" id=\"pass\" class=\"form-control\" name=\"pass\" [(ngModel)]=\"user.password\" placeholder=\"Password\" required/>\n      <br/>\n      <button class=\"btn btn-primary\" type=\"Submit\">Sign in</button>\n    </form>\n    <div *ngIf=wrongUsernameOrPass class=\"alert alert-warning box-msg\" role=\"alert\">\n      Incorrect credentials.\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/login/login.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var Observable_1 = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var LoginComponent = (function () {
    function LoginComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.user = {};
        this.wrongUsernameOrPass = false;
    }
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authenticationService.login(this.user.username, this.user.password).subscribe(function (loggedIn) {
            if (loggedIn) {
                _this.router.navigate(["/menu"]);
            }
        }, function (err) {
            if (err.toString() === "Illegal login") {
                _this.wrongUsernameOrPass = true;
            }
            else {
                Observable_1.Observable.throw(err);
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: "app-login",
            template: __webpack_require__("../../../../../src/app/components/login/login.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_service_1.AuthenticationService,
            router_1.Router])
    ], LoginComponent);
    return LoginComponent;
}());
exports.LoginComponent = LoginComponent;


/***/ }),

/***/ "../../../../../src/app/components/news-list-item/news-list-item.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/news-list-item/news-list-item.component.html":
/***/ (function(module, exports) {

module.exports = "<p>Da li radis?</p>\n<!-- <div *ngIf=\"news\" class=\"row\">\n  \n  <h2>{{news.title}}</h2>\n  <p>{{news.content}}</p>\n\n</div> -->\n"

/***/ }),

/***/ "../../../../../src/app/components/news-list-item/news-list-item.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var NewsListItemComponent = (function () {
    function NewsListItemComponent() {
    }
    NewsListItemComponent.prototype.ngOnInit = function () {
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], NewsListItemComponent.prototype, "news", void 0);
    NewsListItemComponent = __decorate([
        core_1.Component({
            selector: 'app-news-list-item',
            template: __webpack_require__("../../../../../src/app/components/news-list-item/news-list-item.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/news-list-item/news-list-item.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NewsListItemComponent);
    return NewsListItemComponent;
}());
exports.NewsListItemComponent = NewsListItemComponent;


/***/ }),

/***/ "../../../../../src/app/components/news-list/news-list.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/news-list/news-list.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n  <div class=\"container\" *ngIf=\"page\">\n    <div class=\"inline\">\n      <div class=\"col-md-12\">\n        <div class=\"row\">\n           <app-filter (filterNews)=\"search($event)\"></app-filter>    \n        </div>\n        <div class=\"button-group\">\n          <label class=\"text-center\">Vesti po stranici</label>\n          <select [(ngModel)]=\"itemsPerPage\" (change)=\"itemsPerPageChanged($event.target.value)\">\n            <option>5</option>\n            <option>10</option>\n            <option>20</option>\n            <option>40</option>\n          </select>\n          <label class=\"text-center\">Trenutna stranica: {{currentPageNumber + 1}}/{{totalPages}} </label>\n          <button class=\"btn btn-sm btn-success\" *ngIf=\"isLoggedIn() && isAdmin()\" [routerLink]=\"['/add']\">Dodaj vest</button>\n          <button class=\"btn btn-sm btn-primary pull-right\" [disabled]=\"currentPageNumber>=page.totalPages-1\" (click)=\"changePage(1)\">\n            <span class=\"glyphicon glyphicon-forward\"></span>\n          </button>\n          <button class=\"btn btn-sm btn-primary pull-right\" [disabled]=\"currentPageNumber<1\" (click)=\"changePage(-1)\">\n            <span class=\"glyphicon glyphicon-backward\"></span>\n          </button>\n        </div>\n\n        <table class=\"table table-striped\">\n          <thead>\n            <tr>\n              <th>Kategorija</th>\n              <th>Naslov</th>\n              <th>Opis</th>\n              <th *ngIf=\"isLoggedIn()\">Akcija</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let component of page.content\">\n              <td>{{component.category.name}}</td>\n              <td>{{component.title}}</td>\n              <td>{{component.description}} <button class=\"btn btn-info\">detalji</button></td>\n              <td *ngIf=\"isLoggedIn()\" style=\"width: 250px\">\n                <button class=\"btn btn-danger\" *ngIf=\"isAdmin()\" (click)=\"delete(component.id)\">brisanje</button>\n                <button class=\"btn btn-warning\" *ngIf=\"isAdmin()\" [routerLink]=\"['/edit', component.id]\">izmena</button>\n              </td>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>"

/***/ }),

/***/ "../../../../../src/app/components/news-list/news-list.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var news_service_service_1 = __webpack_require__("../../../../../src/app/services/news-service.service.ts");
var NewsListComponent = (function () {
    function NewsListComponent(authService, router, newsService) {
        this.authService = authService;
        this.router = router;
        this.newsService = newsService;
        this.itemsPerPage = 10;
        this.filter = {
            title: "",
            categoryId: 0
        };
    }
    NewsListComponent.prototype.ngOnInit = function () {
        this.currentPageNumber = 0;
        this.loadData();
    };
    NewsListComponent.prototype.search = function (filter) {
        var _this = this;
        console.log(filter);
        this.filter = filter;
        this.newsService.getAll(this.currentPageNumber, this.itemsPerPage, filter.title, filter.categoryId).subscribe(function (data) {
            _this.page = data;
            _this.totalPages = data.totalPages;
        });
    };
    NewsListComponent.prototype.loadData = function () {
        // this.newsService.getAll(this.currentPageNumber,this.itemsPerPage,this.filter.title,this.filter.categoryId).subscribe(data => {
        //   this.page=data;
        //   this.totalPages=data.totalPages;
        // })
        this.search(this.filter);
    };
    NewsListComponent.prototype.delete = function (id) {
        var _this = this;
        this.newsService.delete(id).subscribe(function () { return _this.loadData(); });
    };
    NewsListComponent.prototype.changePage = function (i) {
        this.currentPageNumber += i;
        this.loadData();
    };
    NewsListComponent.prototype.itemsPerPageChanged = function (i) {
        this.itemsPerPage = i;
        this.loadData();
    };
    NewsListComponent.prototype.isLoggedIn = function () {
        return this.authService.isLoggedIn();
    };
    NewsListComponent.prototype.isAdmin = function () {
        return this.authService.isAdmin();
    };
    NewsListComponent = __decorate([
        core_1.Component({
            selector: 'app-news-list',
            template: __webpack_require__("../../../../../src/app/components/news-list/news-list.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/news-list/news-list.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_service_1.AuthenticationService,
            router_1.Router,
            news_service_service_1.NewsService])
    ], NewsListComponent);
    return NewsListComponent;
}());
exports.NewsListComponent = NewsListComponent;


/***/ }),

/***/ "../../../../../src/app/components/register/register.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"container-fluid\">\n  <div class=\"row\">\n    <form class=\"form-signin\" (ngSubmit)=\"register()\">\n      <label for=\"username\" class=\"sr-only\">Username: </label>\n      <input type=\"text\" id=\"username\" class=\"form-control\" name=\"username\" [(ngModel)]=\"user.username\" placeholder=\"Username\"\n        required autofocus/>\n      <label for=\"pass\" class=\"sr-only\">Password</label>\n      <br/>\n      <input type=\"password\" id=\"pass\" class=\"form-control\" name=\"pass\" [(ngModel)]=\"user.password\" placeholder=\"Password\" required/>\n      <br/>\n      <button class=\"btn btn-primary\" type=\"Submit\">Register</button>\n    </form>\n    <div *ngIf=wrongUsernameOrPass class=\"alert alert-warning box-msg\" role=\"alert\">\n      Incorrect credentials.\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/components/register/register.component.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var Observable_1 = __webpack_require__("../../../../rxjs/_esm5/Observable.js");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var RegisterComponent = (function () {
    function RegisterComponent(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.user = {};
    }
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.authenticationService.register(this.user.username, this.user.password).subscribe(function (registered) {
            if (registered) {
                _this.authenticationService.login(_this.user.username, _this.user.password);
                _this.router.navigate(["/components"]);
            }
        }, function (err) {
            if (err.toString() === "Illegal login") {
                _this.message = err.message;
                _this.error = true;
            }
            else {
                Observable_1.Observable.throw(err);
            }
        });
    };
    RegisterComponent = __decorate([
        core_1.Component({
            selector: "app-register",
            template: __webpack_require__("../../../../../src/app/components/register/register.component.html"),
            styles: [__webpack_require__("../../../../../src/app/components/register/register.component.css")]
        }),
        __metadata("design:paramtypes", [authentication_service_service_1.AuthenticationService,
            router_1.Router])
    ], RegisterComponent);
    return RegisterComponent;
}());
exports.RegisterComponent = RegisterComponent;


/***/ }),

/***/ "../../../../../src/app/services/authentication-service.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var Rx_1 = __webpack_require__("../../../../rxjs/_esm5/Rx.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var jwt_utils_service_1 = __webpack_require__("../../../../../src/app/services/jwt-utils.service.ts");
var AuthenticationService = (function () {
    function AuthenticationService(http, jwtUtilsService) {
        this.http = http;
        this.jwtUtilsService = jwtUtilsService;
    }
    AuthenticationService.prototype.login = function (username, password) {
        var _this = this;
        var headers = new http_1.HttpHeaders({ "Content-Type": "application/json" });
        return this.http.post("/api/login", JSON.stringify({ username: username, password: password }), { headers: headers })
            .map(function (res) {
            var token = res && res["token"];
            if (token) {
                localStorage.setItem("currentUser", JSON.stringify({
                    username: username,
                    roles: _this.jwtUtilsService.getRoles(token),
                    token: token
                }));
                return true;
            }
            else {
                return false;
            }
        })
            .catch(function (error) {
            if (error.status === 400) {
                return Rx_1.Observable.throw("Illegal login");
            }
            else {
                return Rx_1.Observable.throw(error.json().error || "Server error");
            }
        });
    };
    AuthenticationService.prototype.register = function (username, password) {
        var _this = this;
        var headers = new http_1.HttpHeaders({ "Content-Type": "application/json" });
        return this.http.post("/api/register", JSON.stringify({ username: username, password: password }), { headers: headers })
            .map(function (res) {
            var token = res && res["token"];
            if (token) {
                localStorage.setItem("currentUser", JSON.stringify({
                    username: username,
                    roles: _this.jwtUtilsService.getRoles(token),
                    token: token
                }));
                return true;
            }
            else {
                return false;
            }
        })
            .catch(function (error) {
            console.log(error);
            if (error.status === 400) {
                return Rx_1.Observable.throw("Couldn't complete the registration");
            }
            else {
                return Rx_1.Observable.throw(error.json().error || "Server error");
            }
        });
    };
    AuthenticationService.prototype.getToken = function () {
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        var token = currentUser && currentUser.token;
        return token ? token : "";
    };
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem("currentUser");
    };
    AuthenticationService.prototype.isLoggedIn = function () {
        if (this.getToken() !== "") {
            return true;
        }
        else {
            return false;
        }
    };
    AuthenticationService.prototype.getCurrentUser = function () {
        if (localStorage.currentUser) {
            return JSON.parse(localStorage.currentUser);
        }
        else {
            return undefined;
        }
    };
    AuthenticationService.prototype.isAdmin = function () {
        var res = this.getCurrentUser();
        if (res) {
            return res.roles.indexOf("Admin") >= 0;
        }
        return false;
    };
    AuthenticationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient, jwt_utils_service_1.JwtUtilsService])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;


/***/ }),

/***/ "../../../../../src/app/services/can-activate-auth.guard.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var router_1 = __webpack_require__("../../../router/esm5/router.js");
var CanActivateAuthGuard = (function () {
    function CanActivateAuthGuard(authenticationService, router) {
        this.authenticationService = authenticationService;
        this.router = router;
    }
    CanActivateAuthGuard.prototype.canActivate = function (next, state) {
        if (this.authenticationService.isLoggedIn()) {
            return true;
        }
        else {
            this.router.navigate(["/login"]);
            return false;
        }
    };
    CanActivateAuthGuard = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [authentication_service_service_1.AuthenticationService, router_1.Router])
    ], CanActivateAuthGuard);
    return CanActivateAuthGuard;
}());
exports.CanActivateAuthGuard = CanActivateAuthGuard;


/***/ }),

/***/ "../../../../../src/app/services/category-service.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var CategoryService = (function () {
    function CategoryService(http) {
        this.http = http;
        this.path = "api/categories";
    }
    CategoryService.prototype.getAll = function () {
        return this.http.get(this.path);
    };
    CategoryService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], CategoryService);
    return CategoryService;
}());
exports.CategoryService = CategoryService;


/***/ }),

/***/ "../../../../../src/app/services/jwt-utils.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var JwtUtilsService = (function () {
    function JwtUtilsService() {
    }
    JwtUtilsService.prototype.getRoles = function (token) {
        var jwtData = token.split('.')[1];
        var decodedJwtJsonData = window.atob(jwtData);
        var decodedJwtData = JSON.parse(decodedJwtJsonData);
        return decodedJwtData.roles.map(function (x) { return x.authority; }) || [];
    };
    JwtUtilsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], JwtUtilsService);
    return JwtUtilsService;
}());
exports.JwtUtilsService = JwtUtilsService;


/***/ }),

/***/ "../../../../../src/app/services/news-service.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var http_1 = __webpack_require__("../../../common/esm5/http.js");
var NewsService = (function () {
    function NewsService(http) {
        this.http = http;
        this.path = "api/news";
    }
    NewsService.prototype.getAll = function (page, newsPerPage, title, catId) {
        var params = new http_1.HttpParams();
        params = params.append("page", page.toString());
        params = params.append("size", newsPerPage.toString());
        params = params.append("title", title);
        params = params.append("id", catId.toString());
        return this.http.get(this.path, { params: params });
    };
    NewsService.prototype.get = function (id) {
        return this.http.get(this.path + "/" + id);
    };
    NewsService.prototype.save = function (news) {
        var params = new http_1.HttpParams();
        params = params.append("Content-Type", "application/json");
        return news.id === undefined ?
            this.http.post(this.path, news, { params: params })
            : this.http.put(this.path + "/" + news.id, news, { params: params });
    };
    NewsService.prototype.delete = function (id) {
        return this.http.delete(this.path + "/" + id);
    };
    NewsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], NewsService);
    return NewsService;
}());
exports.NewsService = NewsService;


/***/ }),

/***/ "../../../../../src/app/services/token-interceptor.service.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var authentication_service_service_1 = __webpack_require__("../../../../../src/app/services/authentication-service.service.ts");
var core_2 = __webpack_require__("../../../core/esm5/core.js");
var TokenInterceptorService = (function () {
    function TokenInterceptorService(inj) {
        this.inj = inj;
    }
    TokenInterceptorService.prototype.intercept = function (request, next) {
        var authenticationService = this.inj.get(authentication_service_service_1.AuthenticationService);
        request = request.clone({
            setHeaders: {
                "X-Auth-Token": "" + authenticationService.getToken()
            }
        });
        return next.handle(request);
    };
    TokenInterceptorService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_2.Injector])
    ], TokenInterceptorService);
    return TokenInterceptorService;
}());
exports.TokenInterceptorService = TokenInterceptorService;


/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
Object.defineProperty(exports, "__esModule", { value: true });
exports.environment = {
    production: false
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = __webpack_require__("../../../core/esm5/core.js");
var platform_browser_dynamic_1 = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
var app_module_1 = __webpack_require__("../../../../../src/app/app.module.ts");
var environment_1 = __webpack_require__("../../../../../src/environments/environment.ts");
if (environment_1.environment.production) {
    core_1.enableProdMode();
}
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(app_module_1.AppModule)
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map