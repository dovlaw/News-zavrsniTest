package project.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import project.model.News;

@Component
public interface NewsRepository extends JpaRepository<News, Long>{

	public List<News> findByCategoryId(Long id);
	public Page<News> getByTitleContains(Pageable page,String title);
	public Page<News> getByTitleContainsAndCategoryId(Pageable page,String title,Long id);
	
}
