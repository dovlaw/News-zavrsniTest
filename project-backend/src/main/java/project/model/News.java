package project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

import project.model.security.SecurityUser;

@Entity
public class News {

	@Id
	@GeneratedValue
	private Long id;

	private String title;

	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	private String description;

	private String content;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private SecurityUser securityUser;
	
	@OneToMany(mappedBy = "news", fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
	private Set<Comment> comments = new HashSet<>();

	public News() {

	}

	public News(Long id, String title, Category category, String description, String content, SecurityUser securityUser) {
		super();
		this.id = id;
		this.title = title;
		this.category = category;
		this.description = description;
		this.content = content;
		this.securityUser = securityUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public SecurityUser getSecurityUser() {
		return securityUser;
	}

	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

}
