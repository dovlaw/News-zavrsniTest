package project.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

import project.model.security.SecurityUser;

@Entity
public class Comment {

	@Id
	@GeneratedValue
	private Long id;
	
	private String text;
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private News news;
	
	@JsonIgnore
	@ManyToOne(fetch = FetchType.LAZY)
	private SecurityUser securityUser;

	public Comment() {
	
	}

	public Comment(Long id, String text, News news, SecurityUser securityUser) {
		super();
		this.id = id;
		this.text = text;
		this.news = news;
		this.securityUser = securityUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public SecurityUser getSecurityUser() {
		return securityUser;
	}

	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}
	
	
	
	
	
}
