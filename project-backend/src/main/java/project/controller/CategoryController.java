package project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import project.model.Category;
import project.service.CategoryService;


@RestController
public class CategoryController {

	@Autowired
	CategoryService categoryService;

//	@PreAuthorize("isAuthenticated()")
	@GetMapping(value = "api/categories")
	public ResponseEntity<List<Category>> get() {
		final List<Category> categories = categoryService.findAll();
		return new ResponseEntity<>(categories, HttpStatus.OK);
	}

//	@PreAuthorize("isAuthenticated()")
	@GetMapping(value = "/api/categories/{id}")
	public ResponseEntity<Category> getOne(@PathVariable Long id) {
		Category category = categoryService.findOne(id);
		return Optional.ofNullable(category).map(c -> new ResponseEntity<>(category, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
}
