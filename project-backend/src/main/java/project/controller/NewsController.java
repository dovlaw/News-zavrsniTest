package project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import project.dto.ResponseDTO;
import project.model.News;
import project.service.NewsService;

@RestController
public class NewsController {
	
	@Autowired
	NewsService newsService;
	
	@GetMapping(value = "/api/news")
	public ResponseEntity<Page<News>> getNewsPage(Pageable page,String title,Long id) {
		Page<News> news = newsService.findAll(page,title,id);
		

		return new ResponseEntity<Page<News>>(news, HttpStatus.OK);
	}

	@GetMapping(value = "/api/news/{id}")
	public ResponseEntity<News> getOne(@PathVariable Long id) {
		News project = newsService.findOne(id);
		return Optional.ofNullable(project).map(p -> new ResponseEntity<>(project, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}

	@PreAuthorize("isAuthenticated()")
	@PostMapping(value = "/api/news", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<News> create(@RequestBody News recNews) {

		News news = new News();
		news.setTitle(recNews.getTitle());
		news.setCategory(recNews.getCategory());
		news.setDescription(recNews.getDescription());

		return new ResponseEntity<>(newsService.save(news), HttpStatus.CREATED);
	}

	@PreAuthorize("hasAnyAuthority('Admin')")
	@DeleteMapping(value = "/api/news/{id}")
	public ResponseEntity<ResponseDTO> delete(@PathVariable Long id) {
		News news = newsService.findOne(id);

		if (news != null) {
			newsService.delete(id);
			return new ResponseEntity<>(new ResponseDTO("OK"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new ResponseDTO("NOT FOUND"), HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping(value = "/api/news/{id}")
	public ResponseEntity<News> update(@PathVariable Long id, @RequestBody News recNews) {
		News news = newsService.findOne(id);

		if (news != null) {
			
			news.setTitle(recNews.getTitle());
			news.setCategory(recNews.getCategory());
			news.setDescription(recNews.getDescription());

			newsService.save(news);
			return new ResponseEntity<News>(news, HttpStatus.OK);
		} else {
			return new ResponseEntity<News>(HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping(value = "/api/news/category/{catId}")
	public ResponseEntity<List<News>> getNewsByCategory(@PathVariable Long catId) {
		List<News> news = newsService.findByCategoryId(catId);
		return Optional.ofNullable(news).map(p -> new ResponseEntity<>(news, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));

	}
}
