package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import project.model.News;
import project.repository.NewsRepository;

@Component
public class NewsService {
	@Autowired
	NewsRepository newsRepository;
	
	public List<News> findAll() {
		return newsRepository.findAll();
	}

	public Page<News> findAll(Pageable page, String title,Long id) {
		if(id ==0 )
		return newsRepository.getByTitleContains(page, title);
		else {
			return newsRepository.getByTitleContainsAndCategoryId(page, title, id);
		}
	}

	public News findOne(Long id) {
		return newsRepository.findOne(id);

	}

	public void delete(Long id) {
		newsRepository.delete(id);
	}

	public News save(News news) {
		return newsRepository.save(news);
	}

	public List<News> findByCategoryId(Long catId) {
		return newsRepository.findByCategoryId(catId);
	}

//	 public List<News> getByTitleContains(String title) {
//	 return newsRepository.getByTitleContains(title);
//	 }

}
