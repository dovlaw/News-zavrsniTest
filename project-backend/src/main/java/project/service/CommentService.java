package project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import project.model.Comment;
import project.repository.CommentRepository;

@Component
public class CommentService {
	
	@Autowired
	CommentRepository commentRepository;
	
	public List<Comment> findAll() {
		return commentRepository.findAll();
	}
	public Comment findOne(Long id) {
		return commentRepository.findOne(id);
	}

	public Comment save(Comment comment) {
		return commentRepository.save(comment);
	}

	public void remove(Long id) {
		commentRepository.delete(id);
	}
}
