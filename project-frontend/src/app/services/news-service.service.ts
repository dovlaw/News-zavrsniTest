import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Rx";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { JwtUtilsService } from "./jwt-utils.service";
import { Page } from "../models/Page";
import { News } from "../models/News";

@Injectable()
export class NewsService {
  private readonly path = "api/news";
  constructor(private http: HttpClient) { }

  getAll(page: number, newsPerPage: number,title:string,catId:number): Observable<Page<News>> {
    let params = new HttpParams();
    params = params.append("page", page.toString());
    params = params.append("size", newsPerPage.toString());
    params = params.append("title",title);
    params = params.append("id",catId.toString());

    return this.http.get(this.path, { params: params }) as Observable<Page<News>>;
  }


  get(id: number): Observable<News> {
    return this.http.get(`${this.path}/${id}`) as Observable<News>;
  }

  save(news: News): Observable<News> {
    let params = new HttpParams();
    params = params.append("Content-Type", "application/json");
    return news.id === undefined ?
      this.http.post(this.path, news, { params }) as Observable<News>
      : this.http.put(`${this.path}/${news.id}`, news, { params }) as Observable<News>;

  }

  delete(id: number): any {
    return this.http.delete(`${this.path}/${id}`);
  }
}
