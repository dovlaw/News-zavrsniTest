import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../services/authentication-service.service';
import { Router } from '@angular/router';
import { NewsService } from '../../services/news-service.service';
import { Page } from '../../models/Page';
import { News } from '../../models/News';
import { Filter } from '../../models/FIlter';


@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.css']
})
export class NewsListComponent implements OnInit {
  page:Page<News>;
  currentPageNumber:number;
  totalPages:number;
  itemsPerPage = 10;
  

  filter:Filter={
    title:"",
    categoryId:0
  };
  
  constructor(
    private authService : AuthenticationService,
    private router:Router,
    private newsService:NewsService
  ) { }

  ngOnInit() {
    this.currentPageNumber=0;
    this.loadData();
  }
  
  search(filter:Filter){
    console.log(filter);
    this.filter=filter
    this.newsService.getAll(this.currentPageNumber,this.itemsPerPage,filter.title,filter.categoryId).subscribe(data => {
      this.page=data;
      this.totalPages=data.totalPages;

    })
  }

  loadData(){
    // this.newsService.getAll(this.currentPageNumber,this.itemsPerPage,this.filter.title,this.filter.categoryId).subscribe(data => {
    //   this.page=data;
    //   this.totalPages=data.totalPages;
    // })
    this.search(this.filter);
  }
  delete(id:number){
    this.newsService.delete(id).subscribe(()=> this.loadData())
  }

  changePage(i:number){
    this.currentPageNumber+=i;
    this.loadData();
  }
  itemsPerPageChanged(i:number){
    this.itemsPerPage=i;
    this.loadData();
  }
  isLoggedIn(): boolean{
    return this.authService.isLoggedIn();
  }
  isAdmin(){
    return this.authService.isAdmin();
  }
}
