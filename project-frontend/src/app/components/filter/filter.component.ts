import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Input } from "@angular/core";
import { NewsListComponent } from "../news-list/news-list.component";
import { NewsService } from "../../services/news-service.service";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";
import { Filter } from "../../models/Filter";
import { CategoryService } from "../../services/category-service.service";
import { Category } from "../../models/Category";




@Component({
  selector: "app-filter",
  templateUrl: "./filter.component.html",
  styleUrls: ["./filter.component.css"],
})
export class FilterComponent implements OnInit {

   @Output() filterNews: EventEmitter<Filter> = new EventEmitter();

    filter: Filter = {
    title: "",
    categoryId: 0
  }
  categories: Category[];
  dataLoaded = false;
  constructor(private newsService: NewsService,
    private categoryService: CategoryService

  ) {
  }

  ngOnInit() {
    this.loadData();
  }
  loadData() {
    this.categoryService.getAll().subscribe(data => {
      this.categories = data;
      this.dataLoaded = true;
    }
    );

  }

  selectionChanged() {
    this.filterNews.next(this.filter);

  }

 
}
