import { Component as c, OnInit } from "@angular/core";

import { AuthenticationService } from "../../services/authentication-service.service";
import { Observable } from "rxjs/Observable";
import { Input } from "@angular/core";
import { Output } from "@angular/core";
import { EventEmitter } from "@angular/core";

import { Router, ActivatedRoute } from "@angular/router";
import { Page } from "../../models/Page";
import { News } from "../../models/News";
import { Category } from "../../models/Category";
import { CategoryService } from "../../services/category-service.service";
import { NewsService } from "../../services/news-service.service";


@c({
  selector: "app-edit",
  templateUrl: "./edit.component.html",
  styleUrls: ["./edit.component.css"]
})
export class Edit {
  news: News
  dataLoaded = false;
  categories: Category[];


  constructor(
    private categoryService: CategoryService,
    private newsService: NewsService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.categoryService.getAll().subscribe((data) => this.categories = data)
    this.route.params.subscribe(data => {
      this.newsService.get(data["id"]).subscribe(data => {
        this.news = data;
        this.dataLoaded = true;
      });
    });

  }

  save() {
    this.newsService.save(this.news).subscribe(
      (yes) => {
        
    this.router.navigate(["/menu"]);
    
      },
      (no) => {
        alert("Error");
      }
    );
  }

  byId(new1: Category, new2: Category) {
    if (new1 && new2) {
      return new1.id === new2.id;
    }
  }

}

