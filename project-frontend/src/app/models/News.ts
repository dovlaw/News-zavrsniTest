import { Category } from "./Category";


export interface News{
    id?: number;
    category: Category;
    title: string;
    description: string;
    content: string;
}