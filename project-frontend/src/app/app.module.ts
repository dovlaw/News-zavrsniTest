import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpModule } from "@angular/http";
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { LoginComponent } from "./components/login/login.component";
import { RegisterComponent } from "./components/register/register.component";
import { Add } from "./components/add/add.component";


import { AuthenticationService } from "./services/authentication-service.service";
import { JwtUtilsService } from "./services/jwt-utils.service";
import { TokenInterceptorService } from "./services/token-interceptor.service";
import { CanActivateAuthGuard } from "./services/can-activate-auth.guard";
import { NewsService } from "./services/news-service.service";
import { CategoryService } from "./services/category-service.service";
import { NewsListComponent } from "./components/news-list/news-list.component";
import { Edit } from "./components/edit/edit.component";
import { FilterComponent } from "./components/filter/filter.component";
import { NewsListItemComponent } from './components/news-list-item/news-list-item.component';


const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "menu", component: NewsListComponent },
  { path: "add", component: Add, canActivate: [CanActivateAuthGuard] },
  { path: "edit/:id", component: Edit, canActivate: [CanActivateAuthGuard] },
  { path: "", redirectTo: "menu", pathMatch: "full" },
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NewsListComponent,
    Add,
     Edit,
    FilterComponent,
    NewsListItemComponent
     
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes,
      {
        enableTracing: false
      }
    )
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
    },
    AuthenticationService,
    CanActivateAuthGuard,
    JwtUtilsService,
    CategoryService,
    NewsService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
